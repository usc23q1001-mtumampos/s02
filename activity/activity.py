name = "Mikael"
age = 22
occupation = "student"
movieTitle = "Pirates of the Caribbean: The Curse of the Black Pearl"
rating = 100
print(f'I am {name}, and I am {age} years old. I am a {occupation}, and my rating for "{movieTitle}" is {rating}%.')

num1 = 3
num2 = 6
num3 = 9
print(num1 * num2)
print(num1 < num3)
num2 = num2 + num3
print(num2)